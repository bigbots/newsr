# README #

In order to get this application running perform the following steps.

* Clone the repository to your local machine.
* This project uses CocoaPods for third party libraries. There is a podfile included in the root of the project. Install the podfile in terminal with the usual command 'pod install' from the root of the new project.
* Once the podfile is installed, the installation will create a file called, 'Newsr.xcworkspace'. Open up the project in XCode using this file.
* Thats it! The Newsr app should now build and run!