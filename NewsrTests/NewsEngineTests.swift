//
//  NewsrKitTests.swift
//  Newsr
//
//  Created by Tommy on 6/23/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import CoreData
import XCTest
import Newsr
import NewsrKit

class NewsEngineTests: XCTestCase {

    var rssUrl = "http://news.google.com/?output=rss"
    var newsItems = [NewsItem]()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }


    func testGetNewsItems() {
        
        // Because getNewsItems is an async call we need to create an Expectation to wait for the response
        let getNewsExpectation = expectationWithDescription("test get news items")
        
        NewsEngine.sharedInstance.getNewsItems(rssUrl) { (newsResponseObject:[NewsItem]?, newsError:NSError?) in
            
            if (newsError == nil) {
                self.newsItems = newsResponseObject!
                
                XCTAssertNotNil(self.newsItems, "newsItems is not nil")
                XCTAssert(self.newsItems.count > 0, "NewsItems were successfully loaded")
                
            } else {
                XCTFail("An error occurred loading NewsItems")
            }
            
            getNewsExpectation!.fulfill()
        }
        
        waitForExpectationsWithTimeout(5.0, handler: nil)
    }
    
    func testGetSourceFromTitle() {
        let title = "This is a news article headline - Buckeye News"
        let source = NewsEngine.sharedInstance.getSourceFromTitle(title)
        
        XCTAssertEqual(source, "Buckeye News", "The source was generated correctly")
    }
    
    func testLoadHtmlForNewsItems() {
        
        // Setup the news items that we are going to load html for
        let news1 = NewsItem(
            title: "Iran deal reached, Obama hails step toward 'more hopeful world' - Reuters",
            source: "Reuters",
            summary: "Iran deal reached, Obama hails step toward's a more hopeful world",
            storyUrl: "http://news.google.com/news/url?sa=t&fd=R&ct2=us&usg=AFQjCNEt9VtbiADh8Sj5A5ny7gWcLjBDQw&clid=c3a7d30bb8a4878e06b80cf16b898331&cid=52778900981319&ei=viWlVej4BM7J3gHAtarwDQ&url=http://www.reuters.com/article/2015/07/14/us-iran-nuclear-idUSKCN0PM0CE20150714")
        let news2 = NewsItem(
            title: "Where is 'El Chapo' Guzman? His history holds clues - Los Angeles Times",
            source: "Los Angeles Times"	,
            summary: "Where is El Chapo Guzman? His history holds clues.",
            storyUrl: "http://news.google.com/news/url?sa=t&fd=R&ct2=us&usg=AFQjCNE6BaNfEZnuJNk38Vk3WygMP05pzw&clid=c3a7d30bb8a4878e06b80cf16b898331&cid=52778901453863&ei=yCylVfinOYnn3AHGxoKoBA&url=http://abcnews.go.com/US/wireStory/man-accused-calif-abduction-called-hoax-32432743")
        
        newsItems.append(news1)
        newsItems.append(news2)
        
        // Because loadHtmlForNewsItems is an async call we need to create an Expectation to wait for the response
        let loadHtmlExpectation = expectationWithDescription("test to load HTML for news item")
        
        NewsEngine.sharedInstance.loadHtmlForNewsItems(self.newsItems) { (htmlResponseObject:[NewsItem]?, htmlError:NSError?) in
            if (htmlError == nil) {
                self.newsItems = htmlResponseObject!
                
                XCTAssertNotNil(self.newsItems, "newsItems is not nil")
                XCTAssertEqual(self.newsItems.count, 2, "Expected NewsItem count is correct")
                
                XCTAssert(self.newsItems[0].storyHtml!.length > 0 , "HTML for story 1 was successfully loaded")
                XCTAssertNotNil(self.newsItems[0].image, "UIImage for story 1 is loaded")
                XCTAssert(self.newsItems[1].storyHtml!.length > 0 , "HTML for story 2 was successfully loaded")
                XCTAssertNotNil(self.newsItems[1].image, "UIImage for story 2 is loaded")

            } else {
                XCTFail("An error occurred loading HTML for news items")
            }
            
            loadHtmlExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(15.0, handler: nil)
    }
    
    func testLoadImage() {
        
        let image = NewsEngine.sharedInstance.loadImage("http://s2.reutersmedia.net/resources/media/global/assets/images/20150714/20150714_1462938920150714134418.jpg")
        
        XCTAssertNotNil(image, "Image was loaded successfully")
    }
}
