//
//  MasterViewControllerTests.swift
//  Newsr
//
//  Created by Tommy on 6/23/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import UIKit
import CoreData
import XCTest
import Newsr
import NewsrKit

class MasterViewControllerTests: XCTestCase {

    var viewController: MasterViewController!
    var storeCoordinator: NSPersistentStoreCoordinator!
    var managedObjectContext: NSManagedObjectContext!
    var managedObjectModel: NSManagedObjectModel!
    var store: NSPersistentStore!
    
    override func setUp() {
        super.setUp()
        
        viewController = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType)).instantiateViewControllerWithIdentifier("MasterViewController") as! MasterViewController
        
        managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
        storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        store = storeCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil, error: nil)
        
        managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = storeCoordinator
        
        viewController.newsrData = NewsrData(context: managedObjectContext)
        viewController.loadView()
    }
    
    override func tearDown() {
        managedObjectContext = nil
        
        var error: NSError? = nil
        XCTAssert(storeCoordinator.removePersistentStore(store, error: &error), "couldn't remove persistent store: \(error)")
        
        super.tearDown()
    }


    func testMasterLoadsWithTableView() {
        var testViewController = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType)).instantiateViewControllerWithIdentifier("MasterViewController") as! MasterViewController
        
        testViewController.loadView()

        XCTAssertTrue(testViewController.tableView != nil, "The table view should be set")
    }
    
    func testGetRssUrl() {
        let url = viewController.getRssUrl()
        
        XCTAssertNotNil(url, "Url is not nil")
        XCTAssertEqual(url!, "http://news.google.com/?output=rss", "The RssUrl has been set properly")
    }
    
    func testSaveNewsItems() {
        var newsItems = [NewsItem]()
        let news1 = NewsItem(title: "Headline 1", source: "Source 1", summary: "Summary 1", storyUrl: "http://story1.com")
        let news2 = NewsItem(title: "Headline 2", source: "Source 2", summary: "Summary 2", storyUrl: "http://story2.com")
        let news3 = NewsItem(title: "Headline 3", source: "Source 3", summary: "Summary 3", storyUrl: "http://story3.com")
    
        newsItems.append(news1)
        newsItems.append(news2)
        newsItems.append(news3)
        
        viewController.saveNewsItems(newsItems)
        
        let loadedNewsItems = viewController.newsrData?.loadNewsItems()
        
        XCTAssertNotNil(loadedNewsItems, "loadedNewsItems is not nil")
        XCTAssertEqual(loadedNewsItems!.count, 3, "The correct amount of NewsItems were loaded")
    }

}
