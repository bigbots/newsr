//
//  NewsrDataTests.swift
//  Newsr
//
//  Created by Tommy on 6/22/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import CoreData
import XCTest
import Newsr
import NewsrKit

class NewsrDataTests: XCTestCase {

    var storeCoordinator: NSPersistentStoreCoordinator!
    var managedObjectContext: NSManagedObjectContext!
    var managedObjectModel: NSManagedObjectModel!
    var store: NSPersistentStore!
    var newsItems: [NewsItem]!
    
    override func setUp() {
        super.setUp()

        // Setup the in-memory object context
        managedObjectModel = NSManagedObjectModel.mergedModelFromBundles(nil)
        storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        store = storeCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil, error: nil)
        
        managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = storeCoordinator
        
        // Setup the newsItems with dummy data
        newsItems = [NewsItem]()
        let news1 = NewsItem(title: "Headline 1", source: "Source 1", summary: "Summary 1", storyUrl: "http://story1.com")
        let news2 = NewsItem(title: "Headline 2", source: "Source 2", summary: "Summary 2", storyUrl: "http://story2.com")
        let news3 = NewsItem(title: "Headline 3", source: "Source 3", summary: "Summary 3", storyUrl: "http://story3.com")
        
        newsItems.append(news1)
        newsItems.append(news2)
        newsItems.append(news3)
    }
    
    override func tearDown() {
        managedObjectContext = nil
        
        var error: NSError? = nil
        XCTAssert(storeCoordinator.removePersistentStore(store, error: &error), "couldn't remove persistent store: \(error)")
        
        super.tearDown()
    }

    func testSaveNewsItem() {
        
        var newsrData = NewsrData(context: managedObjectContext)
        newsrData.saveNewsItem(newsItems[0])
        let loadedNewsItems = newsrData.loadNewsItems()
        
        XCTAssertNotNil(loadedNewsItems, "loadedNewsItems is not nil")
        XCTAssertEqual(loadedNewsItems.count, 1, "Expected NewsItem count is correct")
        XCTAssertEqual(loadedNewsItems[0].title, newsItems[0].title , "The original and loaded news title match")
        XCTAssertEqual(loadedNewsItems[0].source, newsItems[0].source , "The original and loaded news source match")
        XCTAssertEqual(loadedNewsItems[0].summary, newsItems[0].summary , "The original and loaded news summary match")
        XCTAssertEqual(loadedNewsItems[0].storyUrl, newsItems[0].storyUrl , "The original and loaded news storyUrl match")
    }
    
    func testLoadNewsItems() {
        
        var newsrData = NewsrData(context: managedObjectContext)
        newsrData.saveNewsItem(newsItems[1])
        newsrData.saveNewsItem(newsItems[2])
        let loadedNewsItems = newsrData.loadNewsItems()
        
        XCTAssertNotNil(loadedNewsItems, "loadedNewsItems is not nil")
        XCTAssertEqual(loadedNewsItems.count, 2, "Expected NewsItem count is correct")
    }
    
    func testClearNewsItems() {
        
        var newsrData = NewsrData(context: managedObjectContext)
        
        // Save the 3 news items
        for newsItem in newsItems {
            newsrData.saveNewsItem(newsItem)
        }
        
        // Load the 3 news items
        var loadedNewsItems = newsrData.loadNewsItems()
        
        XCTAssertEqual(loadedNewsItems.count, 3, "The correct number of news items have been loaded")
        
        newsrData.clearNewsItems()
        loadedNewsItems = newsrData.loadNewsItems()
        
        XCTAssertEqual(loadedNewsItems.count, 0, "The news items have been deleted succesfully")
    }
}
