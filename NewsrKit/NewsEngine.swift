//
//  NewsEngine.swift
//  Newsr
//
//  Created by Tommy on 6/25/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import SWXMLHash

public typealias NewsItemsResponse = ([NewsItem]?, NSError?) -> Void

public class NewsEngine: NSObject {
    
    public class var sharedInstance:NewsEngine {
        struct Singleton {
            static let instance = NewsEngine()
        }
        return Singleton.instance
    }
    
    public func getNewsItems(sourceUrl: String, onCompletion: NewsItemsResponse) {
        var newsItems = [NewsItem]()
        
        Alamofire.request(.GET, sourceUrl, parameters: nil).responseString{
            (request, response, data, error) in
            
            if (error == nil) {
                
                var xml = SWXMLHash.parse(data!)
               
                for elem in xml["rss"]["channel"]["item"] {
                    var news = NewsItem(
                        title: elem["title"].element!.text!,
                        source: self.getSourceFromTitle(elem["title"].element!.text!),
                        summary: HtmlFormater.removeHtmlTags(elem["description"].element!.text!),
                        storyUrl: elem["link"].element!.text!
                    )
                    newsItems.append(news)
                }
            
                onCompletion(newsItems, nil)
                
            } else {
                onCompletion(nil, error)
            }
        }
    }
    
    public func getSourceFromTitle(title: String) -> String {
        var source = String()
        
        var index = title.rangeOfString("-", options: .BackwardsSearch)?.startIndex.successor()
        
        if index != nil {
            source = title.substringFromIndex(index!).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        }
        
        return source
    }
    
    public func loadHtmlForNewsItems(newsItems: [NewsItem], onCompletion: NewsItemsResponse) {
        var errors = [NSError]()
        let group = dispatch_group_create()
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

        // Block this thread until all tasks are complete
        dispatch_group_wait(group, DISPATCH_TIME_FOREVER)
        
        var itemsDictionary = [String:String]()

        for item in newsItems {
            dispatch_group_enter(group)
                
            itemsDictionary[item.storyUrl] = ""
                
            Alamofire.request(.GET, item.storyUrl, parameters: nil).responseString {
                (request, response, data, error) in
                    
                if (error == nil) {
                    itemsDictionary[request.URLString] = data
                } else {
                    errors.append(error!)
                }
                    
                dispatch_group_leave(group)
            }
        }
                
        // This block will be executed when all tasks are complete
        dispatch_group_notify(group, queue) {
    
            for news in newsItems {
                let storyHtml = itemsDictionary[news.storyUrl]
                let imageUrl = HtmlFormater.getImageUrl(storyHtml!)
                
                news.storyHtml = storyHtml!
                news.imageUrl = self.fixRelativePath(news.storyUrl, relativeUrl: imageUrl)
                news.image = self.loadImage(news.imageUrl!)
            }
            
            if (errors.count == 0) {
                onCompletion(newsItems, nil)
            } else {
                onCompletion(nil, errors[0])
            }
        }
    }
    
    private func fixRelativePath(fullUrl: String, relativeUrl: String) -> String {
        
        if relativeUrl.length > 0 {
            let firstChar = relativeUrl[0]
            
            if firstChar == "/" {
                let url = NSURL(string: fullUrl)
                
                var newUrl = "\(url?.scheme)://\(url?.host)"
                
                if url?.port != nil {
                    newUrl += ":\(url?.port)"
                }
                
                newUrl += relativeUrl
                
                return newUrl
            }
        }
        
        return relativeUrl
    }
    
    public func loadImage(imageUrl: String) -> UIImage? {
        
        let url = NSURL(string: imageUrl)
        var image: UIImage?
                
        if url != nil {
            let data = NSData(contentsOfURL: url!)
                    
            if data != nil {
                image = UIImage(data: data!)!
            }
        }
        
        return image
        
    }
    
}