//
//  Story.swift
//  Newsr
//
//  Created by Tommy on 7/6/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import CoreData

public class Story: NSManagedObject {

    @NSManaged public var title: String
    @NSManaged public var source: String
    @NSManaged public var summary: String
    @NSManaged public var storyUrl: String
    @NSManaged public var storyHtml: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var imageData: NSData?
    
}