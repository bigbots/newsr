//
//  NewsItem.swift
//  Newsr
//
//  Created by Tommy on 6/24/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import UIKit

public class NewsItem: NSObject {
    
    public var title: String
    public var source: String
    public var summary: String
    public var storyUrl: String
    public var imageUrl: String?
    public var storyHtml: String?
    public var image: UIImage?
    
    public init(title: String, source: String, summary: String, storyUrl: String) {
        self.title = title
        self.source = source
        self.summary = summary
        self.storyUrl = storyUrl
        self.storyHtml = nil
        self.imageUrl = nil
        self.image = nil
    }
    
    convenience init(story: Story) {
        self.init(title: story.title, source: story.source, summary: story.summary, storyUrl: story.storyUrl)
        self.storyHtml = story.storyHtml
        self.imageUrl = story.imageUrl
        
        if (story.imageData != nil) {
            self.image = UIImage(data: story.imageData!)
        }
    }
    
}
