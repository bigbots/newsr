//
//  HtmlFormater.swift
//  Newsr
//
//  Created by Tommy on 6/29/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import UIKit

public  class HtmlFormater: NSObject {

    public static func removeHtmlTags(html: String) -> String {
        
        let htmlTagRegex:NSRegularExpression  = NSRegularExpression(
            pattern: "<.*?>",
            options: NSRegularExpressionOptions.CaseInsensitive,
            error: nil)!
        
        let doubleSpaceRegex:NSRegularExpression  = NSRegularExpression(
            pattern: "\\s+",
            options: NSRegularExpressionOptions.CaseInsensitive,
            error: nil)!
        
        let range1 = NSMakeRange(0, count(html))
        let htmlLessString :String = htmlTagRegex.stringByReplacingMatchesInString(html,
            options: NSMatchingOptions.allZeros,
            range:range1 ,
            withTemplate: " ")
        
        let range2 = NSMakeRange(0, count(htmlLessString))
        let singleSpacedString :String = doubleSpaceRegex.stringByReplacingMatchesInString(htmlLessString,
            options: NSMatchingOptions.allZeros,
            range:range2 ,
            withTemplate: " ")
        
        let decodedHtml = decodeHtml(singleSpacedString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))
        
        return decodedHtml
    }
    
    public static func decodeHtml(encodedString: String) -> String {
        let encodedData = encodedString.dataUsingEncoding(NSUTF8StringEncoding)!
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
            NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
        ]
        let attributedString = NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil, error: nil)!
        let decodedString = attributedString.string
        
        return decodedString
    }
    
    public static func getImageUrl(html: String) ->String {
        var imageUrl = String()
        
        let regex: NSRegularExpression = NSRegularExpression(
            pattern: "<img.*?src=\"([^\"]*.jpe?g)\"",
            options: .CaseInsensitive,
            error: nil)!
        
        let range = NSMakeRange(0, count(html))
        
        let urlMatches = regex.matchesInString(html, options: nil, range: range) as! [NSTextCheckingResult]
        
        if urlMatches.count > 0 {
            
            var index = 0
            var urls = [String]()
            var maxLength = 0
            
            for match in urlMatches {
                urls.append((html as NSString).substringWithRange(match.rangeAtIndex(1)))
                index++
            }
            
            if urls.count > 0 {
                imageUrl = urls[0]
            }
        }
        
        return imageUrl
    }
}
