//
//  NewsrData.swift
//  Newsr
//
//  Created by Tommy on 7/12/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import CoreData
import UIKit

public class NewsrData: NSObject {
    
    public var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    public func saveNewsItem(newsItem: NewsItem) {
        
        let entity = NSEntityDescription.entityForName("Story", inManagedObjectContext: context)
        let story = Story(entity: entity!,insertIntoManagedObjectContext: context)
        
        story.title = newsItem.title
        story.source = newsItem.source
        story.summary = newsItem.summary
        story.storyUrl = newsItem.storyUrl
        story.storyHtml = newsItem.storyHtml
        story.imageUrl = newsItem.imageUrl
        
        if newsItem.image != nil {
            story.imageData = UIImageJPEGRepresentation(newsItem.image, CGFloat())
        } else {
            story.imageData = nil
        }
        
        
        context.save(nil)
    }
    
    public func clearNewsItems() {
        
        let request = NSFetchRequest(entityName: "Story")
        let stories = context.executeFetchRequest(request,error: nil) as! [Story]
        
        for story in stories {
            context.deleteObject(story)
        }
    }
    
    public func loadNewsItems() -> [NewsItem] {
        
        var news = [NewsItem]()
        let request = NSFetchRequest(entityName: "Story")
        let stories = context.executeFetchRequest(request,error: nil) as! [Story]
        
        for story in stories {
            let newsItem = NewsItem(story: story)
            news.append(newsItem)
        }
        
        return news
    }
}
