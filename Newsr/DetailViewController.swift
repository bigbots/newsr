//
//  DetailViewController.swift
//  Newsr
//
//  Created by Tommy on 6/22/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import UIKit
import WebKit
import NewsrKit

class DetailViewController: UIViewController {

//    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    func configureView() {
        
        if self.webView != nil {
            // Update the user interface for the detail item.
            if let detail: AnyObject = self.detailItem {
                if let newsItem = detail as? NewsItem {
                
                    self.title = newsItem.source
                    
                    if let html = newsItem.storyHtml {
                        self.webView!.loadHTMLString(html, baseURL: nil)
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

