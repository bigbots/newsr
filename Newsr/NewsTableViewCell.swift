//
//  NewsTableViewCell.swift
//  Newsr
//
//  Created by Tommy on 6/23/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import UIKit
import NewsrKit
import Alamofire

class NewsTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var storyTitleLabel: UILabel!
    @IBOutlet weak var storySummaryLabel: UILabel!
    
    // MARK: - Properties
    var newsItem: NewsItem? {
        didSet {
            configureCell()
        }
    }
    
    // MARK: - Utility Methods
    private func configureCell() {
        storyImageView.image = newsItem?.image
        storyTitleLabel.text = newsItem?.title
        storySummaryLabel.text = newsItem?.summary
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
