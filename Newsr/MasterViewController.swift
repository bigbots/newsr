//
//  MasterViewController.swift
//  Newsr
//
//  Created by Tommy on 6/22/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import MBProgressHUD
import Alamofire
import SWXMLHash
import NewsrKit

class MasterViewController: UITableViewController {

    var newsrData: NewsrData?
    var newsItems = [NewsItem]()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if Connectivity.isConnectedToNetwork() {
            // if we are connected clear out cached news items
            newsrData!.clearNewsItems()
            // now load new news stories
            loadRssFeed()
        } else {
            // if there is no internet connection load news from previous session
            self.newsItems = newsrData!.loadNewsItems()
            
            if self.newsItems.count == 0 {
                showAlert("Currently Offline", message: "You are currently offline with no cached news. Connect to the Internet to get the latest news stories.")
            } else {
                showAlert("Currently Offline", message: "You are currently offline. Tap 'OK' to view cached news stories.")
                self.tableView.reloadData()
            }
        }
        
    }
    
    func loadRssFeed() {
        
        showHud()
        
        if let rssUrl = getRssUrl() {
            
            NewsEngine.sharedInstance.getNewsItems(rssUrl) { (newsResponseObject:[NewsItem]?, newsError:NSError?) in
                
                if (newsError == nil) {
                    self.newsItems = newsResponseObject!
                    
                    NewsEngine.sharedInstance.loadHtmlForNewsItems(self.newsItems) { (htmlResponseObject:[NewsItem]?, htmlError:NSError?) in
                        if (htmlError == nil) {
                            self.newsItems = htmlResponseObject!
                            
                            dispatch_async(dispatch_get_main_queue()) {
                                self.hideHud()
                                self.saveNewsItems(self.newsItems)
                                self.tableView.reloadData()
                            }
                        } else {
                            self.hideHud()
                            self.showAlert("Unexpected Error", message: "An error occurred while gathering the news. Please close Newsr and try again.")
                        }
                    }
                } else {
                    self.hideHud()
                    self.showAlert("Unexpected Error", message: "An error occurred while gathering the news. Please close Newsr and try again.")
                }
            }
        } else {
            hideHud()
            showAlert("Something's not right", message: "Newsr hasn't been configured correctly to get the news!")
        }
    }
    
    func getRssUrl() -> String? {
        let path = NSBundle.mainBundle().pathForResource("Newsr", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let url = dict!.valueForKey("FeedUrl") as? String
        
        return url
    }
    
    func showHud() {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Loading Top Stories"
    }
    
    func hideHud() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveNewsItems(newsItems: [NewsItem]) {
        for item in newsItems {
            newsrData!.saveNewsItem(item)
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                
                let newsItem = self.newsItems[indexPath.row]
                (segue.destinationViewController as! DetailViewController).detailItem = newsItem
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsItems.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! NewsTableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    func configureCell(cell: NewsTableViewCell, atIndexPath indexPath: NSIndexPath) {
        
        cell.storyTitleLabel.text = newsItems[indexPath.row].title
        cell.storySummaryLabel.text = newsItems[indexPath.row].summary
        
        if let image = newsItems[indexPath.row].image {
            cell.storyImageView.image = image
        }
       
        if let imageUrl = newsItems[indexPath.row].imageUrl {
            let url = NSURL(string: imageUrl)
            
            if url != nil {
                let data = NSData(contentsOfURL: url!)
                
                if data != nil {
                    let image = UIImage(data: data!)
                    cell.storyImageView.image = image
                }
            }
            
        }
    }

}

