//
//  xmlResponseSerializer.swift
//  Newsr
//
//  Created by Tommy on 6/26/15.
//  Copyright (c) 2015 Big Bots Corporation. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

extension Alamofire.Request {
    class func xmlResponseSerializer() -> Serializer {
        return { request, response, data in
            if data == nil {
                return (nil, nil)
            }
            
            var xml = SWXMLHash.parse(data!)
            
            return (xml as? AnyObject, nil)
        }
    }
    
    func responseXml(completionHandler: (NSURLRequest, NSHTTPURLResponse?, XMLIndexer, NSError?) -> Void) -> Self {
        return response(serializer: Request.xmlResponseSerializer(), completionHandler: { (request, response, xml, error) in
            completionHandler(request, response, xml as! XMLIndexer, error)
        })
    }
}